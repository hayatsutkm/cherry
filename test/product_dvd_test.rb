require 'minitest\autorun'
require_relative '../lib/product.rb'
#require_relative '../lib/dvd.rb'

class ProductTest < Minitest::Test
  def test_product_initialize
    product = Product.new("A great movie", 1000)
    assert_equal 'A great movie', product.name
    assert_equal 1000, product.price  
  end
  def test_dvd_initialize
    dvd = DVD.new("An awesome film", 1500, 120)
    assert_equal 'An awesome film', dvd.name
    assert_equal 1500, dvd.price  
    assert_equal 120, dvd.running_time  
  end
end