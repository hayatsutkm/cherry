def introduce
  puts "こんにちは"
  puts "私はにんじゃわんこです"
end

introduce
#----------------------------------
def introduce(name)
  puts "こんにちは"
  puts "私は#{name}です"
end

introduce("にんじゃわんこ")
introduce("ひつじ仙人")
#-----------------------------------
def introduce(name, age)
  puts "私は#{name}です"
  puts "#{age}歳です"
end
introduce("にんじゃわんこ", 14)
#--------------------------------------
def add(a,b)
  return a + b
end
sum = add(1,3)
puts sum
puts ("---------------------------------------")
def negative?(number)
  return number < 0
end
puts negative?(5)
puts ("----------------------------------------")
def introduce(name:,age:,food:)
  puts "私は#{name}です"
  puts "年齢は#{age}です"
  puts "好きな食べ物は#{food}です"
end
introduce(name:"にんじゃわんこ",age:14,food:"ラーメン")
