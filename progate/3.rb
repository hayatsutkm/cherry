names = ["Suzuki","Kato","Tanaka"]
puts names
puts names[1]
names.each do |name|
  puts "名前は#{name}です"
end
#-----------------------------------------
user = {name: "Suzuki", age: 21}
puts user[:name]
#------------------------------------
users = [
  {name: "Suzuki", age: 21},
  {name: "Kato", age: 14}
]
puts users[1][:name]
#----------------------------------
users.each do |user|
  puts user[:name]
end