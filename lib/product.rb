class Product
  attr_accessor :name, :price

  def initialize(name, price)
    @name = name
    @price = price
  end

  def to_s
    "name: #{@name}, price: #{@price}"
  end
end

class DVD < Product
  attr_accessor :running_time

  def initialize(name, price, runing_time)
    super(name, price)
    @running_time = runing_time
  end

  def to_s
    "#{super}, runnig_time: #{running_time}"
  end
end

product = Product.new("A great movie", 1000)
puts "product:#{product.name.rjust(product.name.length)} \nprice  :#{product.price.to_s.rjust(product.name.length)}"
puts "\n"
dvd = DVD.new("An awesome film", 1500, 120)
puts "product     :#{dvd.name.rjust(dvd.name.length)} \nprice       :#{dvd.price.to_s.rjust(dvd.name.length)} \nrunning_time:#{dvd.running_time.to_s.rjust(dvd.name.length)}"
puts product.to_s
puts dvd.to_s
