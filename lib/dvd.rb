class DVD < Product
  attr_accessor :running_time

  def initialize(name, price, runing_time)
    super(name, price)
    @running_time = runing_time
  end
end
dvd = DVD.new("An awesome film", 1500, 120)
puts "product     :#{dvd.name.rjust(dvd.name.length)} \nprice       :#{dvd.price.to_s.rjust(dvd.name.length)} \nrunning_time:#{dvd.running_time.to_s.rjust(dvd.name.length)}"