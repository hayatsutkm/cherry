def to_hex(r,g,b)
  [r,g,b].inject('#') do |hex, n| 
    hex + n.to_s(16).rjust(2,'0')
  end
end

def to_ints(hex)
  hex.scan(/\w\w/).map(&:hex)
end

puts to_hex(199,39,99)
puts to_ints('#123456')